package io.veedata.android;

import android.accounts.Account;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.lang.ref.WeakReference;

import io.veedata.android.band.BandConnectionManager;
import io.veedata.android.band.sensor.SensorListenerService;
import io.veedata.android.band.sensor.SensorListenerTaskResult;
import io.veedata.android.band.sync.SyncUtils;


public class MobileMainActivity extends Activity {

    private static final String LOG_TAG = MobileMainActivity.class.getSimpleName();

    private static final int REQUEST_OAUTH = 1;

    // The authority for the sync adapter's content provider
    public static final String AUTHORITY = "io.veedata.android.band.sync";

    // An account type, in the form of a domain name
    public static final String ACCOUNT_TYPE = "veedata.io";
    // The account name
    public static final String ACCOUNT = "dummyaccount";
    // Instance fields
    Account mAccount;

    /**
     *  Track whether an authorization activity is stacking over the current activity, i.e. when
     *  a known auth error is being resolved, such as showing the account chooser or presenting a
     *  consent dialog. This avoids common duplications as might happen on screen rotations, etc.
     */
    private static final String AUTH_PENDING = "auth_state_pending";
    private static final String TAG = "MobileMainActivity";
    private boolean authInProgress = false;

    private TextView stepsCountTextView;

    private Button btnStart;
    private TextView mTxtConnectionStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_main);

        stepsCountTextView = (TextView) findViewById(R.id.stepsCountText);

        if (savedInstanceState != null) {
            authInProgress = savedInstanceState.getBoolean(AUTH_PENDING);
        }

        mTxtConnectionStatus = (TextView) findViewById(R.id.txtConnectionStatus);

        final WeakReference<Activity> reference = new WeakReference<Activity>(this);

        btnStart = (Button) findViewById(R.id.btnStart);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SensorListenerTask().execute(reference);
            }
        });

        // Create the dummy account
        mAccount = SyncUtils.CreateSyncAccount(this);

/*
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter != null && bluetoothAdapter.isEnabled()) {
            Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
            // If there are paired devices
            if (pairedDevices.size() > 0) {
                // Loop through paired devices
                for (BluetoothDevice device : pairedDevices) {
                    Log.i(LOG_TAG, "Bluetooth device: " + device.getName() + ", address: " + device.getAddress());
                }
            }
        }
*/
    }

    //The BroadcastReceiver that listens for bluetooth broadcasts
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                Log.i(LOG_TAG, "Bluetooth device found: " + device.getName());
            }
            else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                Log.i(LOG_TAG, "Bluetooth device is now connected: " + device.getName());
            }
            else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                Log.i(LOG_TAG, "Bluetooth device search done: " + device.getName());
            }
            else if (BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED.equals(action)) {
                Log.i(LOG_TAG, "Bluetooth device is about to disconnect: " + device.getName());
            }
            else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                Log.i(LOG_TAG, "Bluetooth device has disconnected: " + device.getName());

            } else if (SensorListenerService.REPORT_LISTENING.equals(action)) {
                final String extraMessage = intent.getStringExtra(SensorListenerService.REPORT_MESSAGE);
                final String uiMessage = "Listening to Band sensors." + extraMessage;

                Log.i(LOG_TAG, uiMessage);
                appendToConnectionStatusTxt(uiMessage);

            } else if (SensorListenerService.REPORT_STOP_LISTENING.equals(action)) {
                final String extraMessage = intent.getStringExtra(SensorListenerService.REPORT_MESSAGE);
                final String uiMessage = "Stopped listening to Band sensors: " + extraMessage;

                Log.i(LOG_TAG, uiMessage);
                appendToConnectionStatusTxt(uiMessage);

            }
        }
    };

    private void appendToConnectionStatusTxt(final String string) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mTxtConnectionStatus.setText(string);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        registerReceiver(mReceiver, new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED));
        registerReceiver(mReceiver, new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED));
        registerReceiver(mReceiver, new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED));

        registerReceiver(mReceiver, new IntentFilter(SensorListenerService.REPORT_LISTENING));
        registerReceiver(mReceiver, new IntentFilter(SensorListenerService.REPORT_STOP_LISTENING));
    }

    @Override
    protected void onStop() {
        unregisterReceiver(mReceiver);

        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mobile_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class SensorListenerTask extends AsyncTask<WeakReference<Activity>, Void, SensorListenerTaskResult> {
        @Override
        protected SensorListenerTaskResult doInBackground(WeakReference<Activity>... params) {
            return BandConnectionManager.getInstance().startRecording(params[0]);
        }

        @Override
        protected void onPreExecute() {
            appendToConnectionStatusTxt("Connecting to Band...");
        }

        @Override
        protected void onPostExecute(final SensorListenerTaskResult sensorListenerTaskResult) {
            appendToConnectionStatusTxt(sensorListenerTaskResult.getMessage());
        }
    }

}
