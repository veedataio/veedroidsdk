package io.veedata.android.band.data;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by great_000 on 4/18/2016.
 */
public abstract class BandDataContract implements BaseColumns {
    /**
     * Content provider authority.
     */
    public static final String CONTENT_AUTHORITY = "io.veedata.android.band.provider";
    /**
     * Base URI. (content://io.veedata.android.band.provider)
     */
    protected static final Uri BASE_CONTENT_URI = Uri.parse("content://" + BandDataContract.CONTENT_AUTHORITY);
}
