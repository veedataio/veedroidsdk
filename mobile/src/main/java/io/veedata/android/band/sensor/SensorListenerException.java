package io.veedata.android.band.sensor;

/**
 * Created by great_000 on 3/1/2016.
 */
public class SensorListenerException extends Throwable {
    public SensorListenerException(String detailedMessage) {
        super(detailedMessage);
    }

    public SensorListenerException(String detailedMessage, Throwable cause) {
        super(detailedMessage, cause);
    }
}
