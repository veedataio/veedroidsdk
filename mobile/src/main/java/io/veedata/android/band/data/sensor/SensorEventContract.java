package io.veedata.android.band.data.sensor;

/**
 * Created by great_000 on 2/2/2016.
 */
public abstract class SensorEventContract extends io.veedata.android.band.data.BandDataContract {

    /**
     * Name for column "time_stamp" of type TEXT
     */
    public static final String COLUMN_NAME_TIME_STAMP = "time_stamp";

}
