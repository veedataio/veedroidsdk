package io.veedata.android.band.data.session;

import android.net.Uri;

import io.veedata.android.band.data.BandDataContract;

/**
 * Created by great_000 on 4/18/2016.
 */
public class StreamSessionContract extends BandDataContract {
    public static final String TABLE_NAME = "stream_session";

    /**
     * Fully qualified URI for "entry" resources.
     */
    public static final Uri CONTENT_URI =
            BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

    public static final String COLUMN_NAME_START_TIME_STAMP = "start";
    public static final String COLUMN_NAME_END_TIME_STAMP = "end";
}
