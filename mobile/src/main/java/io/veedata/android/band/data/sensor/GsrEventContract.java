package io.veedata.android.band.data.sensor;

import android.net.Uri;

/**
 * Created by great_000 on 2/3/2016.
 */
public class GsrEventContract extends SensorEventContract {
    public static final String COLUMN_NAME_RESISTANCE = "resistance";
    public static final String TABLE_NAME = "gsr";
    public static final Uri CONTENT_URI =
            BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

}
