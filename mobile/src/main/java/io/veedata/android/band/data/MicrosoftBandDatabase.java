package io.veedata.android.band.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import io.veedata.android.band.data.sensor.AccelerometerEventContract;
import io.veedata.android.band.data.sensor.AmbientLightEventContract;
import io.veedata.android.band.data.sensor.BarometerEventContract;
import io.veedata.android.band.data.sensor.ConnectionStateEventContract;
import io.veedata.android.band.data.sensor.ContactStateEventContract;
import io.veedata.android.band.data.sensor.DistanceEventContract;
import io.veedata.android.band.data.sensor.GsrEventContract;
import io.veedata.android.band.data.sensor.GyroscopeEventContract;
import io.veedata.android.band.data.sensor.HeartRateEventContract;
import io.veedata.android.band.data.sensor.RRIntervalEventContract;
import io.veedata.android.band.data.sensor.SkinTemperatureEventContract;
import io.veedata.android.band.data.sensor.UVEventContract;
import io.veedata.android.band.data.session.StreamSessionContract;

/**
 * Created by great_000 on 4/13/2016.
 */
class MicrosoftBandDatabase extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "microsoft_band";
    private static final String TYPE_TEXT = " TEXT";
    private static final String TYPE_INTEGER = " INTEGER";
    private static final String TYPE_NUMERIC = " NUMERIC";
    private static final String TYPE_REAL = " REAL";
    private static final String COMMA_SEP = ",";

    private static final String SQL_CREATE_ACCELEROMETER_TABLE =
            "CREATE TABLE " + AccelerometerEventContract.TABLE_NAME + " (" +
                    AccelerometerEventContract._ID + " INTEGER PRIMARY KEY," +
                    AccelerometerEventContract.COLUMN_NAME_TIME_STAMP + TYPE_TEXT + COMMA_SEP +
                    AccelerometerEventContract.COLUMN_NAME_X + TYPE_REAL + COMMA_SEP +
                    AccelerometerEventContract.COLUMN_NAME_Y + TYPE_REAL + COMMA_SEP +
                    AccelerometerEventContract.COLUMN_NAME_Z + TYPE_REAL + ")";

    private static final String SQL_CREATE_AMBIENT_LIGHT_TABLE =
            "CREATE TABLE " + AmbientLightEventContract.TABLE_NAME + " (" +
                    AmbientLightEventContract._ID + " INTEGER PRIMARY KEY," +
                    AmbientLightEventContract.COLUMN_NAME_TIME_STAMP + TYPE_TEXT + COMMA_SEP +
                    AmbientLightEventContract.COLUMN_NAME_BRIGHTNESS + TYPE_INTEGER + ")";

    private static final String SQL_CREATE_BAROMETER_TABLE =
            "CREATE TABLE " + BarometerEventContract.TABLE_NAME + " (" +
                    BarometerEventContract._ID + " INTEGER PRIMARY KEY," +
                    BarometerEventContract.COLUMN_NAME_TIME_STAMP + TYPE_TEXT + COMMA_SEP +
                    BarometerEventContract.COLUMN_NAME_AIR_PRESSURE + TYPE_REAL + COMMA_SEP +
                    BarometerEventContract.COLUMN_NAME_TEMPERATURE + TYPE_REAL + ")";

    private static final String SQL_CREATE_DISTANCE_TABLE =
            "CREATE TABLE " + DistanceEventContract.TABLE_NAME + " (" +
                    DistanceEventContract._ID + " INTEGER PRIMARY KEY," +
                    DistanceEventContract.COLUMN_NAME_TIME_STAMP + TYPE_TEXT + COMMA_SEP +
                    DistanceEventContract.COLUMN_NAME_MOTION_TYPE + TYPE_TEXT + ")";

    private static final String SQL_CREATE_GSR_TABLE =
            "CREATE TABLE " + GsrEventContract.TABLE_NAME + " (" +
                    GsrEventContract._ID + " INTEGER PRIMARY KEY," +
                    GsrEventContract.COLUMN_NAME_TIME_STAMP + TYPE_TEXT + COMMA_SEP +
                    GsrEventContract.COLUMN_NAME_RESISTANCE + TYPE_INTEGER + ")";

    private static final String SQL_CREATE_GYROSCOPE_TABLE =
            "CREATE TABLE " + GyroscopeEventContract.TABLE_NAME + " (" +
                    GyroscopeEventContract._ID + " INTEGER PRIMARY KEY," +
                    GyroscopeEventContract.COLUMN_NAME_TIME_STAMP + TYPE_TEXT + COMMA_SEP +
                    GyroscopeEventContract.COLUMN_NAME_ACCELERATION_X + TYPE_REAL + COMMA_SEP +
                    GyroscopeEventContract.COLUMN_NAME_ACCELERATION_Y + TYPE_REAL + COMMA_SEP +
                    GyroscopeEventContract.COLUMN_NAME_ACCELERATION_Z + TYPE_REAL + COMMA_SEP +
                    GyroscopeEventContract.COLUMN_NAME_ANGULAR_VELOCITY_X + TYPE_REAL + COMMA_SEP +
                    GyroscopeEventContract.COLUMN_NAME_ANGULAR_VELOCITY_Y + TYPE_REAL + COMMA_SEP +
                    GyroscopeEventContract.COLUMN_NAME_ANGULAR_VELOCITY_Z + TYPE_REAL + ")";

    private static final String SQL_CREATE_HEART_RATE_TABLE =
            "CREATE TABLE " + HeartRateEventContract.TABLE_NAME + " (" +
                    HeartRateEventContract._ID + " INTEGER PRIMARY KEY," +
                    HeartRateEventContract.COLUMN_NAME_TIME_STAMP + TYPE_TEXT + COMMA_SEP +
                    HeartRateEventContract.COLUMN_NAME_HEART_RATE + TYPE_INTEGER + COMMA_SEP +
                    HeartRateEventContract.COLUMN_NAME_QUALITY + TYPE_TEXT + ")";

    private static final String SQL_CREATE_RR_INTERVAL_TABLE =
            "CREATE TABLE " + RRIntervalEventContract.TABLE_NAME + " (" +
                    RRIntervalEventContract._ID + " INTEGER PRIMARY KEY," +
                    RRIntervalEventContract.COLUMN_NAME_TIME_STAMP + TYPE_TEXT + COMMA_SEP +
                    RRIntervalEventContract.COLUMN_NAME_RR_INTERVAL + TYPE_NUMERIC + ")";

    private static final String SQL_CREATE_SKIN_TEMPERATURE_TABLE =
            "CREATE TABLE " + SkinTemperatureEventContract.TABLE_NAME + " (" +
                    SkinTemperatureEventContract._ID + " INTEGER PRIMARY KEY," +
                    SkinTemperatureEventContract.COLUMN_NAME_TIME_STAMP + TYPE_TEXT + COMMA_SEP +
                    SkinTemperatureEventContract.COLUMN_NAME_TEMPERATURE + TYPE_NUMERIC + ")";

    private static final String SQL_CREATE_CONTACT_STATE_TABLE =
            "CREATE TABLE " + ContactStateEventContract.TABLE_NAME + " (" +
                    ContactStateEventContract._ID + " INTEGER PRIMARY KEY," +
                    ContactStateEventContract.COLUMN_NAME_TIME_STAMP + TYPE_TEXT + COMMA_SEP +
                    ContactStateEventContract.COLUMN_NAME_STATE + TYPE_TEXT + ")";

    private static final String SQL_CREATE_CONNECTION_STATE_TABLE =
            "CREATE TABLE " + ConnectionStateEventContract.TABLE_NAME + " (" +
                    ConnectionStateEventContract._ID + " INTEGER PRIMARY KEY," +
                    ConnectionStateEventContract.COLUMN_NAME_TIME_STAMP + TYPE_TEXT + COMMA_SEP +
                    ConnectionStateEventContract.COLUMN_NAME_STATE + TYPE_TEXT + ")";

    private static final String SQL_CREATE_UV_TABLE =
            "CREATE TABLE " + UVEventContract.TABLE_NAME + " (" +
                    UVEventContract._ID + " INTEGER PRIMARY KEY," +
                    UVEventContract.COLUMN_NAME_TIME_STAMP + TYPE_TEXT + COMMA_SEP +
                    UVEventContract.COLUMN_NAME_UV_EXPOSURE_TODAY + TYPE_INTEGER + COMMA_SEP +
                    UVEventContract.COLUMN_NAME_UV_INDEX_LEVEL + TYPE_TEXT + ")";

    private static final String SQL_CREATE_STREAM_SESSION_TABLE =
            "CREATE TABLE " + StreamSessionContract.TABLE_NAME + " (" +
                    StreamSessionContract._ID + " INTEGER PRIMARY KEY," +
                    StreamSessionContract.COLUMN_NAME_START_TIME_STAMP + TYPE_TEXT + COMMA_SEP +
                    StreamSessionContract.COLUMN_NAME_END_TIME_STAMP + TYPE_TEXT + ")";

    public MicrosoftBandDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ACCELEROMETER_TABLE);
        db.execSQL(SQL_CREATE_AMBIENT_LIGHT_TABLE);
        db.execSQL(SQL_CREATE_BAROMETER_TABLE);
        db.execSQL(SQL_CREATE_DISTANCE_TABLE);
        db.execSQL(SQL_CREATE_GSR_TABLE);
        db.execSQL(SQL_CREATE_GYROSCOPE_TABLE);
        db.execSQL(SQL_CREATE_HEART_RATE_TABLE);
        db.execSQL(SQL_CREATE_RR_INTERVAL_TABLE);
        db.execSQL(SQL_CREATE_SKIN_TEMPERATURE_TABLE);
        db.execSQL(SQL_CREATE_CONTACT_STATE_TABLE);
        db.execSQL(SQL_CREATE_CONNECTION_STATE_TABLE);
        db.execSQL(SQL_CREATE_UV_TABLE);
        db.execSQL(SQL_CREATE_STREAM_SESSION_TABLE);
    }

    @Override
    public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
        // do nothing
    }

}
