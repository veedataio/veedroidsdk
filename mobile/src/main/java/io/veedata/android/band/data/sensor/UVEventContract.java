package io.veedata.android.band.data.sensor;

import android.net.Uri;

/**
 * Created by great_000 on 4/13/2016.
 */
public class UVEventContract extends SensorEventContract {
    public static String TABLE_NAME = "uv";
    public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();
    public static final String COLUMN_NAME_UV_EXPOSURE_TODAY = "uv_exposure_today";
    public static final String COLUMN_NAME_UV_INDEX_LEVEL = "uv_index_level";
}
