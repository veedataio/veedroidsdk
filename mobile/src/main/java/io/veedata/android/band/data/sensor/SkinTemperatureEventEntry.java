package io.veedata.android.band.data.sensor;

/**
 * Created by great_000 on 1/27/2016.
 */
public class SkinTemperatureEventEntry extends SensorEventEntry {
    private float temperature;

    public SkinTemperatureEventEntry(String eventTimeStamp, float temperature) {
        super(eventTimeStamp);
        this.temperature = temperature;
    }

    public float getTemperature() {
        return temperature;
    }

}
