package io.veedata.android.band.sensor;

/**
 * Created by great_000 on 3/2/2016.
 */
public class SensorListenerTaskResultError extends SensorListenerTaskResult {
    private Throwable mCause;

    public SensorListenerTaskResultError(final String message) {
        super(message);
    }

    public SensorListenerTaskResultError(final String message, final Throwable cause) {
        super(message);
        mCause = cause;
    }
}
