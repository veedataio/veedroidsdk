package io.veedata.android.band.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by great_000 on 2/1/2016.
 */
public class AuthenticatorService extends Service {
    private static final String LOG_TAG = "AuthenticatorService";

    // Instance field that stores the authenticator object
    private Authenticator mAuthenticator;
    @Override
    public void onCreate() {
        Log.i(LOG_TAG, "onCreate");
        // Create a new authenticator object
        mAuthenticator = new Authenticator(this);
    }
    /*
     * When the system binds to this Service to make the RPC call
     * return the authenticator's IBinder.
     */
    @Override
    public IBinder onBind(Intent intent) {
        Log.i(LOG_TAG, "onBind");
        return mAuthenticator.getIBinder();
    }
}
