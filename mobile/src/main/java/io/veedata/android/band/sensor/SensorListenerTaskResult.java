package io.veedata.android.band.sensor;

/**
 * Created by great_000 on 3/2/2016.
 */
public class SensorListenerTaskResult {

    private String mMessage;

    public SensorListenerTaskResult(final String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }
}
