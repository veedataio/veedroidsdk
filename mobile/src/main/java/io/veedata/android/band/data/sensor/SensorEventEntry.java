package io.veedata.android.band.data.sensor;

/**
 * Created by great_000 on 2/1/2016.
 */
public abstract class SensorEventEntry {
    public String id;
    protected String eventTimestamp;

    public SensorEventEntry(String eventTimestamp) {
        this.eventTimestamp = eventTimestamp;
    }

    public String getEventTimestamp() {
        return eventTimestamp;
    }
}
