package io.veedata.android.band.data.sensor;

import android.net.Uri;

/**
 * Created by great_000 on 3/8/2016.
 */
public class ContactStateEventContract extends SensorEventContract {
    public static final String COLUMN_NAME_STATE = "state";
    public static String TABLE_NAME = "contact_state";
    public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();
}
