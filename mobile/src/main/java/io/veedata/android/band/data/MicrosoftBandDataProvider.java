package io.veedata.android.band.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import io.veedata.android.band.data.sensor.AccelerometerEventContract;
import io.veedata.android.band.data.sensor.AmbientLightEventContract;
import io.veedata.android.band.data.sensor.BarometerEventContract;
import io.veedata.android.band.data.sensor.ConnectionStateEventContract;
import io.veedata.android.band.data.sensor.ContactStateEventContract;
import io.veedata.android.band.data.sensor.DistanceEventContract;
import io.veedata.android.band.data.sensor.GsrEventContract;
import io.veedata.android.band.data.sensor.GyroscopeEventContract;
import io.veedata.android.band.data.sensor.HeartRateEventContract;
import io.veedata.android.band.data.sensor.RRIntervalEventContract;
import io.veedata.android.band.data.sensor.SkinTemperatureEventContract;
import io.veedata.android.band.data.sensor.UVEventContract;
import io.veedata.android.band.data.session.StreamSessionContract;

/**
 * Created by great_000 on 2/1/2016.
 */
public class MicrosoftBandDataProvider extends ContentProvider {

    private static final String LOG_TAG = MicrosoftBandDataProvider.class.getSimpleName();
    /**
     * UriMatcher, used to decode incoming URIs.
     */
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private static final int ROUTE_HEART_RATE = 1;
    private static final int ROUTE_RR_INTERVAL = 2;
    private static final int ROUTE_GSR = 3;
    private static final int ROUTE_SKIN_TEMPERATURE = 4;
    private static final int ROUTE_CONTACT_STATE = 5;
    private static final int ROUTE_CONNECTION_STATE = 6;
    private static final int ROUTE_ACCELEROMETER = 7;
    private static final int ROUTE_AMBIENT_LIGHT = 8;
    private static final int ROUTE_BAROMETER = 9;
    private static final int ROUTE_DISTANCE = 10;
    private static final int ROUTE_GYROSCOPE = 11;
    private static final int ROUTE_UV = 12;
    private static final int ROUTE_STREAM_SESSION = 13;

    static {
        sUriMatcher.addURI(AccelerometerEventContract.CONTENT_AUTHORITY, AccelerometerEventContract.TABLE_NAME, ROUTE_ACCELEROMETER);
        sUriMatcher.addURI(AmbientLightEventContract.CONTENT_AUTHORITY, AmbientLightEventContract.TABLE_NAME, ROUTE_AMBIENT_LIGHT);
        sUriMatcher.addURI(BarometerEventContract.CONTENT_AUTHORITY, BarometerEventContract.TABLE_NAME, ROUTE_BAROMETER);
        sUriMatcher.addURI(DistanceEventContract.CONTENT_AUTHORITY, DistanceEventContract.TABLE_NAME, ROUTE_DISTANCE);
        sUriMatcher.addURI(GsrEventContract.CONTENT_AUTHORITY, GsrEventContract.TABLE_NAME, ROUTE_GSR);
        sUriMatcher.addURI(GyroscopeEventContract.CONTENT_AUTHORITY, GyroscopeEventContract.TABLE_NAME, ROUTE_GYROSCOPE);
        sUriMatcher.addURI(HeartRateEventContract.CONTENT_AUTHORITY, HeartRateEventContract.TABLE_NAME, ROUTE_HEART_RATE);
        sUriMatcher.addURI(RRIntervalEventContract.CONTENT_AUTHORITY, RRIntervalEventContract.TABLE_NAME, ROUTE_RR_INTERVAL);
        sUriMatcher.addURI(SkinTemperatureEventContract.CONTENT_AUTHORITY, SkinTemperatureEventContract.TABLE_NAME, ROUTE_SKIN_TEMPERATURE);
        sUriMatcher.addURI(ContactStateEventContract.CONTENT_AUTHORITY, ContactStateEventContract.TABLE_NAME, ROUTE_CONTACT_STATE);
        sUriMatcher.addURI(ConnectionStateEventContract.CONTENT_AUTHORITY, ConnectionStateEventContract.TABLE_NAME, ROUTE_CONNECTION_STATE);
        sUriMatcher.addURI(UVEventContract.CONTENT_AUTHORITY, UVEventContract.TABLE_NAME, ROUTE_UV);
        sUriMatcher.addURI(StreamSessionContract.CONTENT_AUTHORITY, StreamSessionContract.TABLE_NAME, ROUTE_STREAM_SESSION);
    }

    private SQLiteOpenHelper mMicrosoftBandDatabaseHelper;

    /*
     * Always return true, indicating that the
     * provider loaded correctly.
     */
    @Override
    public boolean onCreate() {
        mMicrosoftBandDatabaseHelper = new MicrosoftBandDatabase(getContext());

        return true;
    }

    /*
     * Return no type for MIME type
     */
    @Override
    public String getType(@NonNull Uri uri) {
        Log.i(LOG_TAG, "getType");
        return null;
    }

    /*
     * query() always returns no results
     *
     */
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Log.d(LOG_TAG, "Querying to uri: " + uri);

        final SQLiteDatabase db = mMicrosoftBandDatabaseHelper.getWritableDatabase();
        assert db != null;

        Cursor cursor = null;
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ROUTE_HEART_RATE:
                cursor = db.query(HeartRateEventContract.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;

            case ROUTE_RR_INTERVAL:
                break;

            case ROUTE_GSR:
                break;

            case ROUTE_SKIN_TEMPERATURE:
                break;

            case ROUTE_CONTACT_STATE:
                break;

            case ROUTE_CONNECTION_STATE:
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        // Note: Notification URI must be manually set here for loaders to correctly
        // register ContentObservers.
        Context ctx = getContext();
        if (ctx == null) throw new AssertionError();
        if (cursor == null) throw new AssertionError();
        cursor.setNotificationUri(ctx.getContentResolver(), uri);

        return cursor;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        Log.d(LOG_TAG, "Inserting to uri: " + uri);

        final SQLiteDatabase db = mMicrosoftBandDatabaseHelper.getWritableDatabase();
        assert db != null;

        long id;
        Uri result;
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ROUTE_ACCELEROMETER:
                id = db.insertOrThrow(AccelerometerEventContract.TABLE_NAME, null, values);
                result = Uri.parse(AccelerometerEventContract.CONTENT_URI + "/" + id);
                break;

            case ROUTE_AMBIENT_LIGHT:
                id = db.insertOrThrow(AmbientLightEventContract.TABLE_NAME, null, values);
                result = Uri.parse(AmbientLightEventContract.CONTENT_URI + "/" + id);
                break;

            case ROUTE_BAROMETER:
                id = db.insertOrThrow(BarometerEventContract.TABLE_NAME, null, values);
                result = Uri.parse(BarometerEventContract.CONTENT_URI + "/" + id);
                break;

            case ROUTE_DISTANCE:
                id = db.insertOrThrow(DistanceEventContract.TABLE_NAME, null, values);
                result = Uri.parse(DistanceEventContract.CONTENT_URI + "/" + id);
                break;

            case ROUTE_GSR:
                id = db.insertOrThrow(GsrEventContract.TABLE_NAME, null, values);
                result = Uri.parse(GsrEventContract.CONTENT_URI + "/" + id);
                break;

            case ROUTE_GYROSCOPE:
                id = db.insertOrThrow(GyroscopeEventContract.TABLE_NAME, null, values);
                result = Uri.parse(GyroscopeEventContract.CONTENT_URI + "/" + id);
                break;

            case ROUTE_HEART_RATE:
                id = db.insertOrThrow(HeartRateEventContract.TABLE_NAME, null, values);
                result = Uri.parse(HeartRateEventContract.CONTENT_URI + "/" + id);
                break;

            case ROUTE_RR_INTERVAL:
                id = db.insertOrThrow(RRIntervalEventContract.TABLE_NAME, null, values);
                result = Uri.parse(RRIntervalEventContract.CONTENT_URI + "/" + id);
                break;

            case ROUTE_SKIN_TEMPERATURE:
                id = db.insertOrThrow(SkinTemperatureEventContract.TABLE_NAME, null, values);
                result = Uri.parse(SkinTemperatureEventContract.CONTENT_URI + "/" + id);
                break;

            case ROUTE_CONTACT_STATE:
                id = db.insertOrThrow(ContactStateEventContract.TABLE_NAME, null, values);
                result = Uri.parse(ContactStateEventContract.CONTENT_URI + "/" + id);
                break;

            case ROUTE_CONNECTION_STATE:
                id = db.insertOrThrow(ConnectionStateEventContract.TABLE_NAME, null, values);
                result = Uri.parse(ConnectionStateEventContract.CONTENT_URI + "/" + id);
                break;

            case ROUTE_UV:
                id = db.insertOrThrow(UVEventContract.TABLE_NAME, null, values);
                result = Uri.parse(UVEventContract.CONTENT_URI + "/" + id);
                break;

            case ROUTE_STREAM_SESSION:
                id = db.insertOrThrow(StreamSessionContract.TABLE_NAME, null, values);
                result = Uri.parse(StreamSessionContract.CONTENT_URI + "/" + id);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        // Send broadcast to registered ContentObservers, to refresh UI.
        Context ctx = getContext();
        assert ctx != null;
        ctx.getContentResolver().notifyChange(uri, null, false);
        return result;
    }

    /*
     * delete() always returns "no rows affected" (0)
     */
    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    /*
     * This method handles only ROUTE_STREAM_SESSION, all other updates will throw an UnsupportedOperationException
     */
    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        Log.d(LOG_TAG, "Updating to uri: " + uri);

        final SQLiteDatabase db = mMicrosoftBandDatabaseHelper.getWritableDatabase();
        assert db != null;

        int rowsAffected;
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ROUTE_STREAM_SESSION:
                rowsAffected = db.update(StreamSessionContract.TABLE_NAME, values, selection, selectionArgs);
                break;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        return rowsAffected;
    }

}