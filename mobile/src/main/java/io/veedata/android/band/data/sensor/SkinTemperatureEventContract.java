package io.veedata.android.band.data.sensor;

import android.net.Uri;

/**
 * Created by great_000 on 2/3/2016.
 */
public class SkinTemperatureEventContract extends SensorEventContract {
    public static final String COLUMN_NAME_TEMPERATURE = "temperature";
    public static String TABLE_NAME = "skin_temperature";
    public static final Uri CONTENT_URI =
            BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();
}
