package io.veedata.android.band.data.sensor;

/**
 * Created by great_000 on 1/27/2016.
 */
public class HeartRateEventEntry extends SensorEventEntry {
    private String id;

    private int heartRate;
    private String quality;

    public HeartRateEventEntry(String eventTimeStamp, int heartRate, String quality) {
        super(eventTimeStamp);
        this.heartRate = heartRate;
        this.quality = quality;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public String getQuality() {
        return quality;
    }
}
