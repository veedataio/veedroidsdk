package io.veedata.android.band.data.sensor;

import android.net.Uri;

/**
 * Created by great_000 on 4/13/2016.
 */
public class AccelerometerEventContract extends SensorEventContract {

    public static final String TABLE_NAME = "accelerometer";

    /**
     * Fully qualified URI for "entry" resources.
     */
    public static final Uri CONTENT_URI =
            BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

    /**
     * Name for column "x-axis" of type DOUBLE
     */
    public static final String COLUMN_NAME_X = "X";

    /**
     * Name for column "y-axis" of type DOUBLE
     */
    public static final String COLUMN_NAME_Y = "Y";

    /**
     * Name for column "z-axis" of type DOUBLE
     */
    public static final String COLUMN_NAME_Z = "Z";
}
