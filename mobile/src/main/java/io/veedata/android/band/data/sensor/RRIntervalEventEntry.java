package io.veedata.android.band.data.sensor;

/**
 * Created by great_000 on 1/27/2016.
 */
public class RRIntervalEventEntry extends SensorEventEntry {
    private double interval;

    public RRIntervalEventEntry(String eventTimeStamp, double interval) {
        super(eventTimeStamp);
        this.interval = interval;
    }

    public double getInterval() {
        return interval;
    }

}
