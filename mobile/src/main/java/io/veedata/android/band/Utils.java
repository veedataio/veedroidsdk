package io.veedata.android.band;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by great_000 on 4/25/2016.
 */
public class Utils {
    public static DateFormat ISO_8601_DATE_TIME = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ");

    static {
//        ISO_8601_DATE_TIME.setTimeZone(TimeZone.getTimeZone("UTC"));
        ISO_8601_DATE_TIME.setTimeZone(TimeZone.getTimeZone("America/Toronto"));
    }

    public static String getISO8601DateTime(final long timeStamp) {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeStamp);
        return ISO_8601_DATE_TIME.format(date);
    }
}
