package io.veedata.android.band;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.microsoft.band.BandClient;
import com.microsoft.band.BandClientManager;
import com.microsoft.band.BandException;
import com.microsoft.band.BandInfo;
import com.microsoft.band.BandPendingResult;
import com.microsoft.band.ConnectionState;
import com.microsoft.band.UserConsent;
import com.microsoft.band.sensors.HeartRateConsentListener;

import java.lang.ref.WeakReference;

import io.veedata.android.band.sensor.SensorListenerException;
import io.veedata.android.band.sensor.SensorListenerManager;
import io.veedata.android.band.sensor.SensorListenerTaskResult;
import io.veedata.android.band.sensor.SensorListenerTaskResultError;

/**
 * Created by great_000 on 4/29/2016.
 */
public class BandConnectionManager {

    private static final String LOG_TAG = BandConnectionManager.class.getSimpleName();

    private static BandConnectionManager mInstance;

    private BandClient mBandClient = null;

    public BandConnectionManager() {
    }

    public static synchronized BandConnectionManager getInstance() {
        if (mInstance == null) {
            mInstance = new BandConnectionManager();
        }

        return mInstance;
    }

    private boolean getConnectedBandClient(final Activity activity) throws InterruptedException, BandException {
        if (mBandClient == null) {
            Log.d(LOG_TAG, "No Band client available, creating one.");

            BandInfo[] devices = BandClientManager.getInstance().getPairedBands();
            if (devices.length == 0) {
                Log.w(LOG_TAG, "Band isn't paired with your phone.\n");
                return false;
            }
            mBandClient = BandClientManager.getInstance().create(activity, devices[0]);

        } else if (ConnectionState.CONNECTED == mBandClient.getConnectionState()) {
            Log.d(LOG_TAG, "Band client is available and connected.");
            return true;
        }

        Log.i(LOG_TAG, "Band client is available and is connecting...");
        final BandPendingResult<ConnectionState> connectionResult = mBandClient.connect();
        final ConnectionState connectionState = connectionResult.await();
        Log.d(LOG_TAG, "Band client connection state after an attempt to connect is: " + connectionState.toString());

        return ConnectionState.CONNECTED == connectionState;
    }

    private class HeartRateConsentTask extends AsyncTask<WeakReference<Activity>, Void, Void> {
        @SafeVarargs
        @Override
        protected final Void doInBackground(WeakReference<Activity>... params) {
            final Activity activity = params[0].get();

            try {
                if (getConnectedBandClient(activity)) {

                    if (activity != null) {
                        Log.d(LOG_TAG, "Requesting user consent");
                        mBandClient.getSensorManager().requestHeartRateConsent(activity, new HeartRateConsentListener() {
                            @Override
                            public void userAccepted(boolean consentGiven) {
                                if (consentGiven) {
                                    SensorListenerManager.startRecording(activity, mBandClient);
                                }
                            }
                        });
                    }
                } else {
                    Log.w(LOG_TAG, "Band isn't connected. Please make sure bluetooth is on and the band is in range.\n");
                }
            } catch (BandException e) {
                String exceptionMessage="";
                switch (e.getErrorType()) {
                    case UNSUPPORTED_SDK_VERSION_ERROR:
                        exceptionMessage = "Microsoft Health BandService doesn't support your SDK Version. Please update to latest SDK.\n";
                        break;
                    case SERVICE_ERROR:
                        exceptionMessage = "Microsoft Health BandService is not available. Please make sure Microsoft Health is installed and that you have the correct permissions.\n";
                        break;
                    default:
                        exceptionMessage = "Unknown error occured: " + e.getMessage() + "\n";
                        break;
                }
                Log.e(LOG_TAG, exceptionMessage, e);

            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage(), e);
            }
            return null;
        }
    }

    private void connectToBandClient(final WeakReference<Activity> reference) throws SensorListenerException {
        /*
         * Here is the process for connecting to the Band Client
         * 1. Connect to the Band client
         * 2. Ask for user's consent to read heart rate
         */
        if (mBandClient == null) {
            BandInfo[] devices = BandClientManager.getInstance().getPairedBands();
            if (devices.length == 0) {
                throw new SensorListenerException("Band isn't paired with your phone.\n");
            }

            mBandClient = BandClientManager.getInstance().create(reference.get(), devices[0]);

        }

        if (mBandClient.getConnectionState() != ConnectionState.CONNECTED) {
            ConnectionState connectionState;
            String connectionFailedMessage = "Band isn't connected. Please make sure bluetooth is on and the band is in range.\n";

            try {
                Log.i(LOG_TAG, "Connecting to Band...\n");
                connectionState = mBandClient.connect().await();

                if (connectionState == ConnectionState.CONNECTED) {
                    Log.i(LOG_TAG, "Band connected\n");
                } else {
                    throw new SensorListenerException(connectionFailedMessage);
                }

            } catch (InterruptedException e) {
                throw new SensorListenerException(connectionFailedMessage, e);

            } catch (BandException e) {
                String exceptionMessage = "";
                switch (e.getErrorType()) {
                    case UNSUPPORTED_SDK_VERSION_ERROR:
                        exceptionMessage = "Microsoft Health BandService doesn't support your SDK Version. Please update to latest SDK.\n";
                        break;
                    case SERVICE_ERROR:
                        exceptionMessage = "Microsoft Health BandService is not available. Please make sure Microsoft Health is installed and that you have the correct permissions.\n";
                        break;
                    default:
                        exceptionMessage = "Unknown error occurred: " + e.getMessage() + "\n";
                }
                throw new SensorListenerException(exceptionMessage, e);
            }

        }
    }

    @NonNull
    public SensorListenerTaskResult startRecording(final WeakReference<Activity> param) {
        try {
            connectToBandClient(param);
        } catch (SensorListenerException e) {
            final String message = "Unable to connect to the Band.";
            Log.e(LOG_TAG, "Unable to connect to the Band.", e);

            return new SensorListenerTaskResultError(message, e);
        }

        if (mBandClient == null) {
            return new SensorListenerTaskResultError("Band client is not available.\n");
        }

        final Activity activity = param.get();
//        if (activity == null) {
//            return new SensorListenerTaskResultError("Band client is not available.\n");
//        }

        final UserConsent currentHeartRateConsent = mBandClient.getSensorManager().getCurrentHeartRateConsent();

        if (currentHeartRateConsent == UserConsent.GRANTED) {
            SensorListenerManager.startRecording(activity, mBandClient);
            return new SensorListenerTaskResult("Sending request to listen to Band sensors.");

        } else {
            mBandClient.getSensorManager().requestHeartRateConsent(activity, new HeartRateConsentListener() {
                @Override
                public void userAccepted(boolean consentGiven) {
                    if (consentGiven) {
                        SensorListenerManager.startRecording(activity, mBandClient);

                    } else {
                        Log.w(LOG_TAG, "User did not give this application consent to access Band heart rate data.");
                    }
                }
            });

            return new SensorListenerTaskResult("Requesting user consent.");
        }



    }

}
