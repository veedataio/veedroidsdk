package io.veedata.android.band.data.sensor;

/**
 * Created by great_000 on 1/27/2016.
 */
public class GsrEventEntry extends SensorEventEntry {
    private int resistance;

    public GsrEventEntry(String eventTimeStamp, int resistance) {
        super(eventTimeStamp);
        this.resistance = resistance;
    }

    public int getResistance() {
        return resistance;
    }
}
