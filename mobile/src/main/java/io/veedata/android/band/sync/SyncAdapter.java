package io.veedata.android.band.sync;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by great_000 on 2/1/2016.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {

    private static final String LOG_TAG = SyncAdapter.class.getSimpleName();
    private static final String STORAGE_CONNECTION_STRING = "DefaultEndpointsProtocol=https;AccountName=veedata;AccountKey=S09Kum6h4FkcXGi6niBVPG8xNHfVJr+wz/LwGL+79hil++TOfKaQYVrSWbQwgk0lpCgCrVB0w8ezpX1jpIwnJg==;BlobEndpoint=https://veedata.blob.core.windows.net/;TableEndpoint=https://veedata.table.core.windows.net/;QueueEndpoint=https://veedata.queue.core.windows.net/;FileEndpoint=https://veedata.file.core.windows.net/";
    private static final String EMPTY_STRING = "";

    // Global variables
    // Define a variable to contain a content resolver instance
    ContentResolver mContentResolver;

    /**
     * Set up the sync adapter
     */
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContentResolver = context.getContentResolver();
    }

    /**
     * Set up the sync adapter. This form of the
     * constructor maintains compatibility with Android 3.0
     * and later platform versions
     */
    public SyncAdapter(
            Context context,
            boolean autoInitialize,
            boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContentResolver = context.getContentResolver();
    }

    /**
     * Perform a sync for this account. SyncAdapter-specific parameters may
     * be specified in extras, which is guaranteed to not be null. Invocations
     * of this method are guaranteed to be serialized.
     *
     * @param account    the account that should be synced
     * @param extras     SyncAdapter-specific parameters
     * @param authority  the authority of this sync request
     * @param provider   a ContentProviderClient that points to the ContentProvider for this
     *                   authority
     * @param syncResult SyncAdapter-specific parameters
     */
    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        Log.i(LOG_TAG, "Beginning network synchronization");
        // TODO implement synchornization

//        HttpURLConnection connection = null;
//
//        try {
//            connection = (HttpURLConnection) new URL("https://veedata.documents.azure.com/dbs/Ua0gAA==/colls/Ua0gAOOBAwA=/docs").openConnection();
////            connection.setDoInput(true);
////            connection.setDoOutput(true);
//            connection.setRequestProperty("Accept", "application/json");
//
//            String verb = "GET";
//            connection.setRequestMethod(verb);
//            // e.g. Tue, 15 Nov 1994 08:12:31 GMT
//            DateTimeFormatter RFC1123_DATE_TIME_FORMATTER =
//                    DateTimeFormat.forPattern("EEE, dd MMM yyyy HH:mm:ss 'GMT'")
//                            .withZoneUTC().withLocale(Locale.CANADA);
//            String xMsDate = DateTime.now().toString(RFC1123_DATE_TIME_FORMATTER);
//            String auth = generateAuthString(verb, xMsDate);
//            connection.setRequestProperty("Authorization", auth);
//            connection.setRequestProperty("x-ms-date", xMsDate);
//            connection.setRequestProperty("x-ms-version", "2015-08-06");
//
//            /*
//             * Sets the versioning of Azure Storage Services and OData Data Service version
//             * Read more:
//             *  - https://msdn.microsoft.com/en-us/library/azure/dd894041.aspx
//             *  - https://msdn.microsoft.com/en-us/library/azure/dn535597.aspx
//             */
////            connection.setRequestProperty("MaxDataServiceVersion", "3.0;NetFx");
////            connection.setRequestProperty("User-Agent", "Apache-HttpClient/4.1.1 (java 1.5)");
//
////            Log.i(LOG_TAG, "Headers: " + Arrays.toString(connection.getHeaderFields().entrySet().toArray()));
//            connection.connect();
//
//            //Write
//            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
//            JSONObject jsonObject = new JSONObject("{ \"TableName\":\"test\" }");
//            String dataString = jsonObject.toString();
//            Log.i(LOG_TAG, dataString);
//            out.write(dataString);
//            out.close();
//            int responseCode = connection.getResponseCode(); //can call this instead of con.connect()
//            if (responseCode >= 400 && responseCode <= 499) {
//                Log.e(LOG_TAG, "Bad authentication status: " + responseCode + " message: " + connection.getResponseMessage());
//
//                Scanner response = new Scanner(connection.getErrorStream(), "UTF-8");
//                while (response.hasNext()) {
//                    String msg = response.next();
//                    Log.i(LOG_TAG, msg);
//                }
//            }
//            else {
//                Scanner response = new Scanner(connection.getInputStream(), "UTF-8");
//                while (response.hasNext()) {
//                    String msg = response.next();
//                    Log.i(LOG_TAG, msg);
//                }
//            }
//
//
//
//        } catch (IOException e) {
//            Log.e(LOG_TAG, e.getMessage(), e);
//
//        } catch (JSONException e) {
//            Log.e(LOG_TAG, e.getMessage(), e);
//
//        } catch (NoSuchAlgorithmException e) {
//            Log.e(LOG_TAG, e.getMessage(), e);
//
//        } catch (InvalidKeyException e) {
//            e.printStackTrace();
//        } finally {
//            if (connection != null) {
//                connection.disconnect();
//            }
//        }
//
//
///*
//        try {
//            Cursor cursor = provider.query(HeartRateEventContract.CONTENT_URI, HeartRateEventContract.PROJECTION_ALL, null, null, HeartRateEventContract.COLUMN_NAME_TIME_STAMP);
//            assert cursor != null;
//
//            cursor.moveToFirst();
//
//            int columnIndex = cursor.getColumnIndexOrThrow(HeartRateEventContract.COLUMN_NAME_TIME_STAMP);
//            String timeStamp = cursor.getString(columnIndex);
//
//            columnIndex = cursor.getColumnIndexOrThrow(HeartRateEventContract.COLUMN_NAME_HEART_RATE);
//            int heartRate = cursor.getInt(columnIndex);
//
//            columnIndex = cursor.getColumnIndexOrThrow(HeartRateEventContract.COLUMN_NAME_QUALITY);
//            String quality = cursor.getString(columnIndex);
//
//            // Setup the cloud storage account.
//            CloudStorageAccount azureAccount = CloudStorageAccount
//                    .parse(STORAGE_CONNECTION_STRING);
//
//            // Create a blob service client
//            CloudTableClient tableClient = azureAccount.createCloudTableClient();
//            CloudTable heartRateTable = tableClient.getTableReference("heart_rate");
//            heartRateTable.createIfNotExists();
//
//            HeartRateEventEntry eventEntry = new HeartRateEventEntry(timeStamp, heartRate, quality);
//            TableOperation tableOperation = TableOperation.insert(eventEntry);
//
//            heartRateTable.execute(tableOperation);
//
//        } catch (RemoteException e) {
//            syncResult.databaseError = true;
//            Log.e(LOG_TAG, e.getMessage());
//
//        } catch (URISyntaxException e) {
//            syncResult.databaseError = true;
//            Log.e(LOG_TAG, e.getMessage());
//
//        } catch (InvalidKeyException e) {
//            syncResult.stats.numAuthExceptions++;
//            Log.e(LOG_TAG, e.getMessage());
//
//        } catch (StorageException e) {
//            syncResult.databaseError = true;
//            Log.e(LOG_TAG, e.getMessage());
//
////        } catch (IOException e) {
////            syncResult.stats.numIoExceptions++;
////            Log.e(LOG_TAG, e.getMessage());
//        }
//*/
//
//
///*
//        try {
//            final URL location = new URL(FEED_URL);
//            InputStream stream = null;
//
//            try {
//                Log.i(LOG_TAG, "Streaming data from network: " + location);
//                stream = downloadUrl(location);
//                updateLocalFeedData(stream, syncResult);
//                // Makes sure that the InputStream is closed after the app is
//                // finished using it.
//            } finally {
//                if (stream != null) {
//                    stream.close();
//                }
//            }
//        } catch (MalformedURLException e) {
//            Log.e(LOG_TAG, "Feed URL is malformed", e);
//            syncResult.stats.numParseExceptions++;
//            return;
//        } catch (IOException e) {
//            Log.e(LOG_TAG, "Error reading from network: " + e.toString());
//            syncResult.stats.numIoExceptions++;
//            return;
//        } catch (XmlPullParserException e) {
//            Log.e(LOG_TAG, "Error parsing feed: " + e.toString());
//            syncResult.stats.numParseExceptions++;
//            return;
//        } catch (ParseException e) {
//            Log.e(LOG_TAG, "Error parsing feed: " + e.toString());
//            syncResult.stats.numParseExceptions++;
//            return;
//        } catch (RemoteException e) {
//            Log.e(LOG_TAG, "Error updating database: " + e.toString());
//            syncResult.databaseError = true;
//            return;
//        } catch (OperationApplicationException e) {
//            Log.e(LOG_TAG, "Error updating database: " + e.toString());
//            syncResult.databaseError = true;
//            return;
//        }
//*/
        Log.i(LOG_TAG, "Network synchronization complete");

    }

    @NonNull
    private String generateAuthString(String verb, String xMsDate) throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {
        String typeOfToken = "master";
        String tokenVersion = "1.0";
        String accessKey = "PyfF5RIHnodDKpRcbxz9algYvtzXd3BASO20cg4ftsgyQydFixvvDTLS7JXer1KdEab3QY/kAONIGVvb6DYvVg==";
        String resourceType = "docs";
//        String resourceId = "dbs/test/colls/testcol";
        String resourceId = "";
        String date = xMsDate;

        /*
        * As per Azure's doco https://msdn.microsoft.com/en-us/library/azure/dd179428.aspx, the
        * String signatures for the Table Service (Shared Key Authorization) follows the format:

          StringToSign = VERB + "\n" +
               Content-MD5 + "\n" +
               Content-Type + "\n" +
               Date + "\n" +
               CanonicalizedResource;
        */
        String stringToSign =
                verb        + "\n" + // method
                resourceType  + "\n" + // Content-MD5
                resourceId + "\n" + // Content-Type
                date  + "\n" + // Date
                "\n";
//        stringToSign = "Sun, 11 Oct 2009 19:52:39 GMT\\n/testaccount1/Tables";
        Log.i(LOG_TAG, "Signing string: " + stringToSign);

        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(new SecretKeySpec(Base64.decode(accessKey, Base64.NO_WRAP), "HmacSHA256"));

        String hashSignature = Base64.encodeToString(mac.doFinal(stringToSign.toLowerCase().getBytes("UTF-8")), Base64.NO_WRAP);
        String auth = URLEncoder.encode("type=" + typeOfToken + "&ver=" + tokenVersion + "&sig=" + hashSignature, "UTF-8");
        Log.i(LOG_TAG, "Authorization value: " + auth);

        return auth;
    }
}