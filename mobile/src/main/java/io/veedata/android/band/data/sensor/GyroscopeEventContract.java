package io.veedata.android.band.data.sensor;

import android.net.Uri;

/**
 * Created by great_000 on 4/13/2016.
 */
public class GyroscopeEventContract extends SensorEventContract {
    public static final String COLUMN_NAME_ACCELERATION_X = "acceleration_x";
    public static final String COLUMN_NAME_ACCELERATION_Y = "acceleration_y";
    public static final String COLUMN_NAME_ACCELERATION_Z = "acceleration_z";
    public static final String COLUMN_NAME_ANGULAR_VELOCITY_X = "ang_velocity_x";
    public static final String COLUMN_NAME_ANGULAR_VELOCITY_Y = "ang_velocity_y";
    public static final String COLUMN_NAME_ANGULAR_VELOCITY_Z = "ang_velocity_z";
    public static String TABLE_NAME = "gyroscope";
    public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();
}
