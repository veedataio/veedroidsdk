package io.veedata.android.band.sensor;

import android.app.IntentService;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.microsoft.band.BandConnectionCallback;
import com.microsoft.band.BandException;
import com.microsoft.band.BandIOException;
import com.microsoft.band.ConnectionState;
import com.microsoft.band.InvalidBandVersionException;
import com.microsoft.band.sensors.BandContactEvent;
import com.microsoft.band.sensors.BandContactEventListener;
import com.microsoft.band.sensors.BandContactState;

import org.joda.time.DateTime;

import io.veedata.android.band.Utils;
import io.veedata.android.band.data.sensor.ConnectionStateEventContract;
import io.veedata.android.band.data.sensor.ContactStateEventContract;
import io.veedata.android.band.data.session.StreamSessionContract;

/**
 * Do not invoke this class directly, use instead SensorListenerManager to start this service.
 * SensorListenerManager performs some preparation work that this service requires.
 */
public class SensorListenerService extends IntentService {
    public static final String ACTION_START_LISTENING = "io.veedata.android.band.data.action.START_LISTENING";
    public static final String REPORT_LISTENING = "io.veedata.android.band.data.report.START";
    public static final String REPORT_STOP_LISTENING = "io.veedata.android.band.data.report.STOP";
    public static final String REPORT_ERROR = "io.veedata.android.band.data.report.ERROR";
    public static final String REPORT_MESSAGE = "io.veedata.android.band.data.report.MESSAGE";

    public static final int ONE_MINUTE = 60000;
    public static final int TEN_SECONDS_PAUSE = 10000;
    public static final int FIVE_MINUTES = 300000;
    public static final int SIX_MINUTES = 360000;

    private static final String LOG_TAG = "SensorListenerService";
    public static int PAUSE_MONITOR_MILLIS = 2000;

    /*
     * The record id for the stream session in progress. Useful for recording the end time of the session.
     */
    private long mStreamSessionId;


    private BandStatusListener mBandStatusListener = new BandStatusListener();
    private SensorEventListener mSensorEventListener = new SensorEventListener(this);
    private Handler mHandler = new Handler();
    private final Runnable mMonitorRunnable = new Runnable() {
        @Override
        public void run() {
            if (mBandStatusListener.isWorn()) {
                if (SensorListenerManager.isListeningHeartRateEvents()) {
                    try {
                        SensorListenerManager.unregisterHeartRateEventListener(mSensorEventListener);
                        SensorListenerManager.unregisterRRIntervalEventListener(mSensorEventListener);

                        Log.d(LOG_TAG, "Stop listening heart rate for " + FIVE_MINUTES + " s.");
                        mHandler.postDelayed(mMonitorRunnable, FIVE_MINUTES);

                    } catch (BandException e) {
                        Log.e(LOG_TAG, "Error unregistering HR listener", e);
                    }
                } else {
                    try {
                        SensorListenerManager.registerHeartRateEventListener(mSensorEventListener);
                        SensorListenerManager.registerRRIntervalEventListener(mSensorEventListener);

                        Log.d(LOG_TAG, "Start listening heart rate for " + SIX_MINUTES + " s.");
                        // pause for a period, then checking away will unregister the HR listener
                        mHandler.postDelayed(mMonitorRunnable, SIX_MINUTES);

                    } catch (BandException | InvalidBandVersionException e) {
                        Log.e(LOG_TAG, "Error registering HR listener", e);
                    }
                }
            } else {
                // wait until the user wears the band again
                mHandler.postDelayed(mMonitorRunnable, FIVE_MINUTES);
            }
        }
    };

    public SensorListenerService() {
        super("SensorListenerService");
    }

    @Override
    public boolean stopService(final Intent name) {
        Log.i(LOG_TAG, "stopService");
        return super.stopService(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_START_LISTENING.equals(action)) {
                try {
                    registerStatusListeners();
                    registerSensorListeners();

                } catch (Exception e) {
                    handleError(e.getMessage());
                }
                mHandler.removeCallbacks(mMonitorRunnable);
                mHandler.post(mMonitorRunnable);

                broadcastMessage(REPORT_LISTENING);
            }
        }
    }

    private void registerStatusListeners() throws BandIOException {
        SensorListenerManager.registerContactEventListener(mBandStatusListener);
        SensorListenerManager.registerConnectionCallbackListener(mBandStatusListener);
    }

    private void registerSensorListeners() throws BandException, InvalidBandVersionException {
        mStreamSessionId = startStreamSession();

//        SensorListenerManager.registerAccelerometerEventListener(mSensorEventListener, SampleRate.MS128);
//        SensorListenerManager.registerAmbientLightEventListener(mSensorEventListener);
        SensorListenerManager.registerBarometerEventListener(mSensorEventListener);
//        SensorListenerManager.registerDistanceEventListener(mSensorEventListener);
//        SensorListenerManager.registerGsrEventListener(mSensorEventListener);
//        SensorListenerManager.registerGyroscopeEventListener(mSensorEventListener, SampleRate.MS128);
        SensorListenerManager.registerHeartRateEventListener(mSensorEventListener);
        SensorListenerManager.registerRRIntervalEventListener(mSensorEventListener);
//        SensorListenerManager.registerSkinTemperatureEventListener(mSensorEventListener);
//        SensorListenerManager.registerUVEventListener(mSensorEventListener);
    }

    private void unregisterEventListener() throws BandException {
//        SensorListenerManager.unregisterAccelerometerEventListener(mSensorEventListener);
//        SensorListenerManager.unregisterAmbientLightEventListener(mSensorEventListener);
        SensorListenerManager.unregisterBarometerEventListener(mSensorEventListener);
//        SensorListenerManager.unregisterDistanceEventListener(mSensorEventListener);
//        SensorListenerManager.unregisterGsrEventListener(mSensorEventListener);
//        SensorListenerManager.unregisterGyroscopeEventListener(mSensorEventListener);
        SensorListenerManager.unregisterHeartRateEventListener(mSensorEventListener);
        SensorListenerManager.unregisterRRIntervalEventListener(mSensorEventListener);
//        SensorListenerManager.unregisterSkinTemperatureEventListener(mSensorEventListener);
//        SensorListenerManager.unregisterUVEventListener(mSensorEventListener);

        endStreamSession(mStreamSessionId);
    }

    private long startStreamSession() {
        String timeStamp = Utils.getISO8601DateTime(DateTime.now().getMillis());

        ContentValues values = new ContentValues();
        values.put(StreamSessionContract.COLUMN_NAME_START_TIME_STAMP, timeStamp);
        final Uri rowUri = getContentResolver().insert(StreamSessionContract.CONTENT_URI, values);
        return ContentUris.parseId(rowUri);
    }

    private void endStreamSession(final long streamSessionId) {
        String timeStamp = Utils.getISO8601DateTime(DateTime.now().getMillis());

        ContentValues updateValues = new ContentValues();
        updateValues.put(StreamSessionContract.COLUMN_NAME_END_TIME_STAMP, timeStamp);
        final String selectionClause = StreamSessionContract._ID + " = ?";
        final String[] selectionArgs = {String.valueOf(streamSessionId)};

        getContentResolver().update(StreamSessionContract.CONTENT_URI, updateValues, selectionClause, selectionArgs);

    }

    private void handleError(final String message) {
        Log.e(LOG_TAG, "SensorListenerService: " + message);
        broadcastMessage(REPORT_ERROR, message);
    }

    private void broadcastMessage(final String action) {
        Log.d(LOG_TAG, "Broadcasting action: " + action);
        Intent intent = new Intent(action);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void broadcastMessage(final String action, final String message) {
        Log.d(LOG_TAG, "Broadcasting action: " + action + ", message: " + message);
        Intent intent = new Intent(action);
        intent.putExtra(REPORT_MESSAGE, message);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void logWarning(final String msg) {
        Log.w(LOG_TAG, msg);
    }

    private class BandStatusListener implements BandContactEventListener, BandConnectionCallback {
        // Always assume the band is initially worn
        private boolean mIsWorn = true;

        @Override
        public void onBandContactChanged(final BandContactEvent event) {
            if (event == null) {
                logWarning("Received a null event!");
                return;
            }

            String timeStamp = Utils.getISO8601DateTime(event.getTimestamp());

            final BandContactState contactState = event.getContactState();
            Log.d(LOG_TAG, "BandConnectState: " + contactState);

            ContentValues values = new ContentValues();
            values.put(ContactStateEventContract.COLUMN_NAME_TIME_STAMP, timeStamp);
            values.put(ContactStateEventContract.COLUMN_NAME_STATE, contactState.toString());
            getContentResolver().insert(ContactStateEventContract.CONTENT_URI, values);

            try {
                if (contactState == BandContactState.NOT_WORN) {
                    Log.d(LOG_TAG, "Band is not worn, stop listening.");
                    mIsWorn = false;
                    unregisterEventListener();
                    broadcastMessage(REPORT_STOP_LISTENING);

                } else if (contactState == BandContactState.WORN) {
                    Log.d(LOG_TAG, "Band is worn, resume listening.");
                    mIsWorn = true;
                    registerSensorListeners();
                    broadcastMessage(REPORT_LISTENING);
                }
            } catch (BandException e) {
                handleError(e.getMessage());

            } catch (Exception e) {
                handleError(e.getMessage());
            }

        }

        @Override
        public void onStateChanged(final ConnectionState connectionState) {
            Log.d(LOG_TAG, "Band connection state changed to: " + connectionState);

            String timeStamp = Utils.getISO8601DateTime(DateTime.now().getMillis());

            ContentValues values = new ContentValues();
            values.put(ConnectionStateEventContract.COLUMN_NAME_TIME_STAMP, timeStamp);
            values.put(ConnectionStateEventContract.COLUMN_NAME_STATE, connectionState.toString());
            getContentResolver().insert(ConnectionStateEventContract.CONTENT_URI, values);
        }

        public boolean isWorn() {
            return mIsWorn;
        }
    }

}
