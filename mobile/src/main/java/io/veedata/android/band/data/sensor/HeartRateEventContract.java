package io.veedata.android.band.data.sensor;

import android.net.Uri;

/**
 * Created by great_000 on 2/2/2016.
 */
public class HeartRateEventContract extends SensorEventContract {

    public static final String TABLE_NAME = "heart_rate";

    /**
     * Fully qualified URI for "entry" resources.
     */
    public static final Uri CONTENT_URI =
            BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

    /**
     * Name for column "heart_rate" of type INTEGER
     */
    public static final String COLUMN_NAME_HEART_RATE = "heart_rate";

    /**
     * Name for column "quality" of type TEXT
     */
    public static final String COLUMN_NAME_QUALITY = "quality";

    public static final String[] PROJECTION_ALL = { COLUMN_NAME_TIME_STAMP, COLUMN_NAME_HEART_RATE, COLUMN_NAME_QUALITY };

}
