package io.veedata.android.band.data.sensor;

import android.net.Uri;

/**
 * Created by great_000 on 4/13/2016.
 */
public class BarometerEventContract extends SensorEventContract {

    public static final String TABLE_NAME = "barometer";

    /**
     * Fully qualified URI for "entry" resources.
     */
    public static final Uri CONTENT_URI =
            BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

    /**
     * Name for column "air_pressure" of type DOUBLE
     */
    public static final String COLUMN_NAME_AIR_PRESSURE = "air_pressure";

    /**
     * Name for column "temperature" of type DOUBLE
     */
    public static final String COLUMN_NAME_TEMPERATURE = "temperature";
}
