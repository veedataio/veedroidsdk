package io.veedata.android.band.data.sensor;

import android.net.Uri;

/**
 * Created by great_000 on 4/13/2016.
 */
public class AmbientLightEventContract extends SensorEventContract {

    public static final String TABLE_NAME = "ambient_light";

    /**
     * Fully qualified URI for "entry" resources.
     */
    public static final Uri CONTENT_URI =
            BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

    /**
     * Name for column "x-axis" of type INTEGER
     */
    public static final String COLUMN_NAME_BRIGHTNESS = "brightness";
}
